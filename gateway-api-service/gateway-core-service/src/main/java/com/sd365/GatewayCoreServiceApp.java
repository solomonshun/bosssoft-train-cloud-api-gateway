package com.sd365;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.openfeign.EnableFeignClients;
/**
 * @class App
 * @classdesc 网关住服务类主要实现了全局的过滤器，具体的认证和鉴权工作分发给认证和
 * 鉴全服务
 * @author Administrator
 * @date 2021-11-17  12:46
 * @version 1.0.0
 * @see
 * @since
 */
@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients
@RefreshScope
public class GatewayCoreServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(GatewayCoreServiceApp.class,args);
    }
}
