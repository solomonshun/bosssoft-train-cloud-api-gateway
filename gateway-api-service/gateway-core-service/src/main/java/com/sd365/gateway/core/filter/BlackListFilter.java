package com.sd365.gateway.core.filter;


import com.alibaba.fastjson.JSON;
import com.sd365.gateway.core.bo.UserBo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ObjectUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.UnsupportedEncodingException;

/**
 * @author Administrator
 * @version 1.0.0
 * @class BlackListFilter
 * @classdesc
 * @date 2021-11-18  11:58
 * @see
 * @since
 */
@Slf4j
@Component
public class BlackListFilter implements GlobalFilter, Ordered {
    /**
     * 用于访问黑名单
     */
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private BlackOrWhiteToken blackOrWhiteToken;
    @Value("${keypres}")
    private String KEY_PRES;
    private static final String MSG = "您在黑名单中！不允许登录！";

    /**
     * 如果是登录请求则立刻进行黑白名单判断 属于黑名单则终止请求继续访问
     *
     * @param exchange
     * @param chain
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("黑名单过滤器1{}", this);
        if (exchange.getRequest().getURI().toString().contains("/user/login")) {
            MultiValueMap<String, String> userInfo = exchange.getRequest().getQueryParams();
            UserBo userBO = new UserBo(userInfo.getFirst("code"), userInfo.getFirst("password"), null, userInfo.getFirst("tenantId"));
            if (checkBlackList(userBO)) {// 属于黑名单不允许放行 请求应答为未授权
                byte[] bytes = {};
                try {
                    bytes = MSG.getBytes("UTF-8");
                } catch (UnsupportedEncodingException e) {

                }
                log.info("黑名单过滤器2{}", "checkBlackList(userBO)=>true");
                return exchange.getResponse().writeWith(Flux.just(exchange.getResponse().bufferFactory().wrap(bytes)));
            } else {
                log.info("黑名单过滤器3{}", "checkBlackList(userBO)=>false");
                return chain.filter(exchange);
            }
        } else { // 非请求登录
            log.info("黑名单过滤器4{}", "非请求登录");
            return chain.filter(exchange);
        }
    }

    /**
     * 数据来源描述：用户中心增加黑白名单模块，界面增加黑白名后将黑白名单list对象存入redis<br>
     * 本方法主要操作描述： 从redis中取得判断是否存在黑名单存在则终止请求返回应答码否则放行
     *
     * @param userBO
     * @return
     */
    private Boolean checkBlackList(UserBo userBO) {
        // 1: 判断一个用户是否在黑名单中，如果在直接返回
        Object o = redisTemplate.opsForValue().get(KEY_PRES + userBO.getCode());
        if (ObjectUtils.isEmpty(o)) {
            return false;
        } else {
            UserBo blacklist = JSON.parseObject(JSON.toJSONString(o), UserBo.class);
            if (!ObjectUtils.isEmpty(blacklist)) {
                if (blacklist.getType().equals("黑名单")) {
                    return true;
                } else {
                    return false;
                }
            }
            return false;
        }
    }

    @Override
    public int getOrder() {
        return 1;
    }
}
