package com.sd365.gateway.core.limit;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class LimitKeyResolverConfig {
    @Primary
    @Bean(value = "hostAddrKeyResolver")
    public KeyResolver HostAddrKeyResolver() {
        return new HostAddrKeyResolver();
    }

    @Bean(value = "uriKeyResolver")
    public KeyResolver uriKeyResolver() {
        return new UriKeyResolver();
    }

    @Bean("UserKeyResolver")
    public KeyResolver userKeyResolver() {
        return new UserKeyResolver();
    }
}
