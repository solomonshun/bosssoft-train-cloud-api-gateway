package com.sd365.gateway.core.limit;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;

import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @class HostAndKeyResolver
 * @classdesc 依据请求参数中的用户工号限流量
 * @author Administrator
 * @date 2021-11-18  20:12
 * @version 1.0.0
 * @see
 * @since
 */

public class UserKeyResolver implements KeyResolver {
    @Override
    public Mono<String> resolve(ServerWebExchange exchange) {
        return Mono.just(exchange.getRequest().getQueryParams().getFirst("code"));
    }


}
