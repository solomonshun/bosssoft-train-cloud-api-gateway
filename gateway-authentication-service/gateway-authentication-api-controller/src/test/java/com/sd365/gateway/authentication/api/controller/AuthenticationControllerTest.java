/**
 * Copyright (C), -2025, www.bosssof.com.cn
 * @FileName AuthenticationControllerTest.java
 * @Author Administrator
 * @Date 2022-10-13  13:47
 * @Description TODO
 * History:
 * <author> Administrator
 * <time> 2022-10-13  13:47
 * <version> 1.0.0
 * <desc> 该文件主要对用户登录的 认证服务的认证接口的测试用例进行要求
 *  验收，程序通过功能测试+单元测试的执行结果综合评定开发者的开发成果
 */
package com.sd365.gateway.authentication.api.controller;

import com.alibaba.fastjson.JSON;
import com.sd365.common.util.TokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
/**
 * @Class AuthenticationControllerTest
 * @Description
 * @Author Administrator
 * @Date 2022-10-13  13:47
 * @version 1.0.0
 */

@Slf4j
class AuthenticationControllerTest {

    /**
     * @Description:
     * 1、网关配置转发请求/user/loing 到 /user/token ,该方法主要是对认证服务的
     * 认证结果生成进行测试
     * 2、该方法进行单元测试时候必须保证 用户中心的auth（）接口能被调用
     * 3、因为用户中心的auth()方法已经进行过单元测试，所以本次测试聚焦在token是否正确上
     * @Author: Administrator
     * @DATE: 2022-10-13  13:47
     */
    @Test
    void test1ForTrueExample() {
        final String TOKEN_URL = "http://localhost:8082/auth/token?code=%s&account=%s&password=%s";
        // 测试步骤1
        //传入正确的账号、密码和租户
        String code = "100002";
        String password = "sd365sd365";
        String account = "BOSSSOFT";
        RestTemplate restTemplate = new RestTemplate();
        try{
            List<HttpMessageConverter<?>> list = restTemplate.getMessageConverters();
            for (HttpMessageConverter<?> httpMessageConverter : list) {
                if (httpMessageConverter instanceof StringHttpMessageConverter){
                    ((StringHttpMessageConverter)
                            httpMessageConverter).setDefaultCharset(Charset.forName("utf-8"));
                    break;
                }
            }
            //请求其url将返回的json数据以字符串形式存储
            HashMap result = restTemplate.getForObject(String.format(TOKEN_URL, code, account, password), HashMap.class);
            log.info(result.toString());
            //对token进行解密
            Map bodyMap = (Map) JSON.parse(result.get("body").toString());
            String token = bodyMap.get("token").toString();
            log.info(TokenUtil.decryptToken(token).toString());
        }catch (Exception e){
            log.info(e.getMessage());
        }
    }

    @Test
    void test2ForFalsePassword() {
        final String TOKEN_URL = "http://localhost:8082/auth/token?code=%s&account=%s&password=%s";
        // 测试步骤2
        // 测试正确的账号和租户，错误的密码
        String code = "100002";
        String password = "sd365sd3651111111111111111";
        String account = "BOSSSOFT";
        RestTemplate restTemplate = new RestTemplate();
        //修改字符集
        try{
            List<HttpMessageConverter<?>> list = restTemplate.getMessageConverters();
            for (HttpMessageConverter<?> httpMessageConverter : list) {
                if (httpMessageConverter instanceof StringHttpMessageConverter){
                    ((StringHttpMessageConverter)
                            httpMessageConverter).setDefaultCharset(Charset.forName("utf-8"));
                    break;
                }
            }
            //请求其url将返回的json数据以字符串形式存储
            HashMap result = restTemplate.getForObject(String.format(TOKEN_URL, code, account, password), HashMap.class);
            log.info(result.toString());
        }catch (Exception e){
            log.info(e.getMessage());
        }
    }

    @Test
    void test3ForFalseAccount() {
        final String TOKEN_URL = "http://localhost:8082/auth/token?code=%s&account=%s&password=%s";
        // 测试步骤2
        // 测试正确的账号和密码，错误的租户
        String code = "100002";
        String password = "sd365sd365";
        String account = "BOSSSOFT11111";
        RestTemplate restTemplate = new RestTemplate();

        //修改字符集
        try{
            List<HttpMessageConverter<?>> list = restTemplate.getMessageConverters();
            for (HttpMessageConverter<?> httpMessageConverter : list) {
                if (httpMessageConverter instanceof StringHttpMessageConverter){
                    ((StringHttpMessageConverter)
                            httpMessageConverter).setDefaultCharset(Charset.forName("utf-8"));
                    break;
                }
            }
            //请求其url将返回的json数据以字符串形式存储
            HashMap result = restTemplate.getForObject(String.format(TOKEN_URL, code, account, password), HashMap.class);
            log.info(result.toString());
        }catch (Exception e){
            log.info(e.getMessage());
        }
    }

    @Test
    void test4ForFalseId() {
        final String TOKEN_URL = "http://localhost:8082/auth/token?code=%s&account=%s&password=%s";
        // 测试步骤2
        // 测试正确的密码和租户，错误的账号
        String code = "100005";
        String password = "sd365sd365";
        String account = "BOSSSOFT";
        RestTemplate restTemplate = new RestTemplate();

        //修改字符集
        try{
            List<HttpMessageConverter<?>> list = restTemplate.getMessageConverters();
            for (HttpMessageConverter<?> httpMessageConverter : list) {
                if (httpMessageConverter instanceof StringHttpMessageConverter){
                    ((StringHttpMessageConverter)
                            httpMessageConverter).setDefaultCharset(Charset.forName("utf-8"));
                    break;
                }
            }
            //请求其url将返回的json数据以字符串形式存储
            HashMap result = restTemplate.getForObject(String.format(TOKEN_URL, code, account, password), HashMap.class);
            log.info(result.toString());
        }catch (Exception e){
            log.info(e.getMessage());
        }
    }

    @Test
    void test5ForTrueExample() {
        final String TOKEN_URL = "http://localhost:8082/auth/token?code=%s&account=%s&password=%s";
        // 测试步骤1
        //传入正确的账号、密码和租户
        String code = "mytest";
        String password = "mytest123";
        String account = "BOSSSOFT";
        RestTemplate restTemplate = new RestTemplate();
        try{
            List<HttpMessageConverter<?>> list = restTemplate.getMessageConverters();
            for (HttpMessageConverter<?> httpMessageConverter : list) {
                if (httpMessageConverter instanceof StringHttpMessageConverter){
                    ((StringHttpMessageConverter)
                            httpMessageConverter).setDefaultCharset(Charset.forName("utf-8"));
                    break;
                }
            }
            //请求其url将返回的json数据以字符串形式存储
            HashMap result = restTemplate.getForObject(String.format(TOKEN_URL, code, account, password), HashMap.class);
            log.info(result.toString());
            //对token进行解密
            Map bodyMap = (Map) JSON.parse(result.get("body").toString());
            String token = bodyMap.get("token").toString();
            log.info(TokenUtil.decryptToken(token).toString());
        }catch (Exception e){
            log.info(e.getMessage());
        }
    }
}