package com.sd365.gateway.authentication.service.impl;

import cn.hutool.http.HttpStatus;
import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.algorithms.Algorithm;
import com.sd365.common.util.TokenUtil;
import com.sd365.gateway.authentication.service.AuthenticationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.TimeUnit;
import com.auth0.jwt.*;

/**
 * @Class AuthenticationServiceImpl
 * @Description 尝试将token生成算法优化
 * @Author solomon
 * @Date 2022-12-13  22:13
 */
@Slf4j
@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    //认证接口的URL
    public static final   String AUTH_URL = "http://sd365-permission-center/permission/centre/v1/user/auth?code=%s&account=%s&password=%s";

    //生成redis的模板对象
    @Resource(name = "tokenRedisTemplate")
    private RedisTemplate redisTemplate;

    //key为string的redis对象模板
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    //生成Rest请求的模板对象
    @Autowired
    private RestTemplate restTemplate;

    //从配置文件获取jwt.period字段为默认有效期
    @Value("${jwt.period}")
    private Long period = 259200000L;

    //一些固定参数的值
    private static final Long ONE_DAY = 86400000L;
    private static final String USER_TOKEN_KEY = "user:token:";

    /**
     * @Description: 前端发起请求后，网关重定向到该函数为用户获取token
     * @param code
     * @param account
     * @param password
     * @return
     */
    @Override
    public String getToken(String code, String account, String password) {

        Assert.hasText(code,"code为空,参数异常");
        Assert.hasText(account,"account为空,参数异常");
        Assert.hasText(password,"password为空,参数异常");

        //创建HttpServletRequest对象，获取当前请求的信息
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes attributes = (ServletRequestAttributes) requestAttributes;
        HttpServletRequest request = attributes.getRequest();
        log.debug("HttpServletRequest request ====>{}", attributes.getRequest());

        //authResultMap为用户中心返回的CommonResponse，其中包含 UserVO对象
        LinkedHashMap authResultMap = null;

        try {
            // 由于restTemplate请求会导致header丢失，所以手动添加header
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.USER_AGENT,request.getHeader("USER-AGENT"));
            //http包含header和body
            HttpEntity entity = new HttpEntity<>(null, headers);
            authResultMap = restTemplate.exchange(String.format(AUTH_URL, code, account, password), HttpMethod.GET, entity, LinkedHashMap.class).getBody();

            if(!authResultMap.isEmpty()){
                //创建token，这里可以选择使用哪个算法来创建（示例算法creatJWTTkoen，个人优化后算法createToken）
                String token = createJWTToken(code, account, authResultMap);

                //这里重写了逻辑，原来的createJWTToken直接返回封装好的json
                //这里改为从createJWTToken获取token字符串，然后在这里封装
                if(token == null){
                    //token为空时，将原authResultMap返回
                    return new JSONObject(authResultMap).toJSONString();
                }
                else{
                    //将token封装回authResultMap
                    final LinkedHashMap body = (LinkedHashMap) authResultMap.get("body");
                    ((LinkedHashMap) (body).get("data")).put("token", token);
                    return new JSONObject(authResultMap).toJSONString();
                }
            }
            else{
                Assert.notEmpty(authResultMap,"authResultMap为空");
                return new JSONObject(authResultMap).toJSONString();
            }

        } catch (Exception ex) {
            log.info("authentication  error", ex);
            throw ex;
        }

    }

    /**
     * @Description: 生成具体的JWT token生成的算法
     * @Author: solomon
     * @DATE: 2022-12-13  20:14
     * @param: 账号密码 以及用户的租户机构公司等信息
     * @return: 生成的JWT TOKEN
     */
    private String createJWTToken(String code, String account, LinkedHashMap authResult) {
        /**
         *  从用户中心返回的authResult中获取生成token所需要的信息
         */

        //获取authResult的body，即UserVO
        final LinkedHashMap body = (LinkedHashMap) authResult.get("body");
        //获取UserVO中的code
        Integer businessCode=(Integer) (body).get("code");

        //根据获取到的code判断是否认证通过，若认证未通过则返回包装成json格式的authResult
        //authResult格式如下{head:{code,message},body:{userVO:{code,message,data:{token } roleids:[]}}}
        if (businessCode != HttpStatus.HTTP_OK) {
            return null;
        }

        //下面依次获取生成token所需的UserVO中的一些参数
        ArrayList roleIds = (ArrayList) (body).get("roleIds");
        Long userId = Long.parseLong((String) body.getOrDefault("id", "-1"));
        Long tenantId = Long.parseLong((String) body.getOrDefault("tenantId", "-1"));
        Long companyId = Long.parseLong((String) body.getOrDefault("companyId", "-1"));
        Long orgId = Long.parseLong((String) body.getOrDefault("orgId", "-1"));
        String userName = (String) body.getOrDefault("name", "-1");


        final JSONObject header = new JSONObject();
        final JSONObject playLoad = new JSONObject();
        //设置header
        header.put("alg", "HS256");
        header.put("typ", "JWT");
        //设置playLoad
        playLoad.put("account", account);
        playLoad.put("roleIds", roleIds);
        playLoad.put("tenantId", tenantId);
        playLoad.put("companyId", companyId);
        playLoad.put("orgId", orgId);
        playLoad.put("userId", userId);
        playLoad.put("userName", userName);
        playLoad.put("code", code);
        //设置过期时间
        playLoad.put("expiresAt",new Date(System.currentTimeMillis() + period));
        String jwtProtocol = header.toJSONString() + "." + playLoad.toJSONString();

        //这里进行代码逻辑的调整，把将token封装回authResult的操作转移到getToken函数中
        //((LinkedHashMap) (body).get("data")).put("token", TokenUtil.encoderToken(jwtProtocol));


        redisTemplate.opsForValue().set(userId.toString(), "userId:" + userId.toString(), 1L, TimeUnit.DAYS);;
        // 设置redis中token时间比实际时间多一个小时
        String encoderToken = TokenUtil.encoderToken(jwtProtocol);
        stringRedisTemplate.opsForValue().set(USER_TOKEN_KEY + userId, encoderToken , period + ONE_DAY, TimeUnit.MILLISECONDS);
        return encoderToken;
    }


    /**
     * @Description: 生成JWT token
     * @Author: solomon
     * @DATE: 2022-12-06  21:17
     * @param: code, account
     * @return: token
     */

    //自定义密钥
    public static final String SECRET_KEY = "bosssoft";

    //token有效时间，单位秒，当前设置规定为一天
    public static final int TOKEN_EXPIRE_TIME = 86400000;
    private String createToken(String code, String account, LinkedHashMap authResult){

        /**
         *  从用户中心返回的authResult中获取生成token所需要的信息
         */
        //获取authResult的body，即UserVO
        final LinkedHashMap body = (LinkedHashMap) authResult.get("body");
        //获取UserVO中的code
        Integer businessCode=(Integer) (body).get("code");

        //根据获取到的code判断是否认证通过，若认证未通过则返回包装成json格式的authResult
        //authResult格式如下{head:{code,message},body:{userVO:{code,message,data:{token } roleids:[]}}}
        if (businessCode != HttpStatus.HTTP_OK) {
            return null;
        }

        //下面依次获取生成token所需的UserVO中的一些参数
        ArrayList roleIds = (ArrayList) (body).get("roleIds");
        Long userId = Long.parseLong((String) body.getOrDefault("id", "-1"));
        Long tenantId = Long.parseLong((String) body.getOrDefault("tenantId", "-1"));
        Long companyId = Long.parseLong((String) body.getOrDefault("companyId", "-1"));
        Long orgId = Long.parseLong((String) body.getOrDefault("orgId", "-1"));
        String userName = (String) body.getOrDefault("name", "-1");

        //设置token过期时间
        Date now = new Date();
        Date date = new Date(now.getTime() + TOKEN_EXPIRE_TIME);

        //设置header信息
        Map<String, Object> head = new HashMap ();
        head.put("alg", "HS256");
        head.put("typ", "JWT");

        //生成token
        String token = JWT.create()
                //设置header
                .withHeader(head)
                //设置playLoad
                .withClaim("account", account)
                .withClaim("roleIds", roleIds)
                .withClaim("tenantId", tenantId)
                .withClaim("companyId", companyId)
                .withClaim("orgId", orgId)
                .withClaim("userId", userId)
                .withClaim("userName", userName)
                .withClaim("code", code)
                //设置过期时间
                .withExpiresAt(date)
                //签名
                .sign(Algorithm.HMAC256(SECRET_KEY));

        return token;
    }
}
