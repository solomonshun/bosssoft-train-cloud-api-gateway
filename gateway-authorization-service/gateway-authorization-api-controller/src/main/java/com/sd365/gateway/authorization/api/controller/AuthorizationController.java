/**
 * Copyright (C), -2025, www.bosssof.com.cn
 * @FileName AuthorizationController.java
 * @Author Administrator
 * @Date 2022-10-13  14:08
 * @Description 网关core服务检测请求如果带有token这转发鉴权服务进行鉴定 该文件所包含类就是实现鉴权相关的业务逻辑
 * History:
 * <author> Administrator
 * <time> 2022-10-13  14:08
 * <version> 1.0.0
 * <desc> 对鉴权服务的代码进行代码规范以及健壮性的优化
 */
package com.sd365.gateway.authorization.api.controller;

import com.sd365.common.core.common.controller.AbstractController;
import com.sd365.common.core.context.BaseContextHolder;
import com.sd365.gateway.authorization.api.Authorization;
import com.sd365.gateway.authorization.service.AuthorizationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
/**
 * @Class AuthorizationController
 * @Description
 * 当前版本主要是实现鉴权方法以及不需要鉴权的资源查询，功能较为单一后续将完善升级
 * @Author Administrator
 * @Date 2022-10-13  14:08
 * @version 1.0.0
 */
@Slf4j
@RestController
public class AuthorizationController extends AbstractController implements Authorization {
    /**
     *  认证业务服务对象包含主要的鉴权逻辑当前版本的功能较为简单
     */
    @Autowired
    private AuthorizationService authorizationService;

    @Override
    public Boolean roleAuthorization(String token, String url) throws InterruptedException {
        log.debug("roleAuthorization url={}, token={},",url,token);
        BaseContextHolder.set("nowrap", new Object());
        return authorizationService.roleAuthorization(token, url);

    }


    @Override
    public Boolean commonResource(String url) throws InterruptedException {
        log.info("roleAuthorization url={}",url);
        BaseContextHolder.set("nowrap", new Object());
        return authorizationService.commonResource(url);
    }
}
