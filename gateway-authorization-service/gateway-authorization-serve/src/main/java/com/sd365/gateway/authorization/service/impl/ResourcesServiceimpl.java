package com.sd365.gateway.authorization.service.impl;

import com.sd365.common.core.annotation.stuffer.IdGenerator;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BusinessErrorCode;
import com.sd365.common.util.BeanException;
import com.sd365.common.util.BeanUtil;
import com.sd365.gateway.authorization.service.ResourcesService;
import com.sd365.gateway.authorization.dao.mapper.ResourceMapper;
import com.sd365.gateway.authorization.entity.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.util.Assert.isTrue;
import static org.springframework.util.Assert.notEmpty;

/**
 * @Author 吴剑鸣
 * @Date 2020/12/18 12:52
 * @Version 1.0
 * @description
 */
@Service
public class ResourcesServiceimpl extends AbstractBaseDataServiceImpl implements ResourcesService {
    @Autowired
    ResourceMapper resourceMapper;

    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    IdGenerator idGenerator;

    @Override
    public Boolean addResource(Resource resource) {
        resource.setId(idGenerator.snowflakeId());
        resource.setVersion(1L);
        super.baseDataStuff4Add(resource);
        super.baseDataStuffCommonPros(resource);
        return resourceMapper.insert(resource)>0;
    }

    @Override
    public Boolean removeResource(Long id) {
        Example example=new Example(Resource.class);
        example.createCriteria().andEqualTo("id",id);
        return resourceMapper.deleteByExample(example)>0;
    }

    @Override
    public List<Resource> searchResource(List<Long> ids) {
        Example example = new Example(Resource.class);
        example.createCriteria().andIn("id", ids);
        return resourceMapper.selectByExample(example);
    }

    @Transactional
    @Override
    public Boolean batchAddResource(List<Resource> resources) {
        try {
            resources.stream().forEach(item -> {
                this.addResource(item);
            });
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Transactional
    @Override
    public Boolean BatchRemoveResource(List<Long> ids) {
        int i = 0;
        List<Resource> resources = new ArrayList<>();
        for(Resource resource: resources){
            resource = this.queryById(ids.get(i));
            resources.add(resource);
            i++;
        }
        try {
            resources.stream().forEach(item ->{
                this.removeResource(item.getId());
            });
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public Resource queryById(Long id) {
        try {
            Resource resource = resourceMapper.selectByPrimaryKey(id);
            return resource;
        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.UNDEFINED, ex);
        }
    }
}
